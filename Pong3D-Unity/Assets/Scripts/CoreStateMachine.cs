﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core
{
	public static class CoreStateMachine
	{
		#if UNITY_ANDROID

		#endif

		#if UNITY_PS4

		#endif

		public enum GAME_STATES { MAIN_MENU = 1, GAMEPLAY, CONSOLE, PAUSE_MENU, INIT_STATE }

		private static GAME_STATES currentGameState = GAME_STATES.INIT_STATE;
		private static SceneLoader sceneLoader = new SceneLoader();
	
		// Update is called once per frame
		public static void Update ()
		{
			checkGameState();
		}

		public static GAME_STATES checkGameState( bool printState = false )
		{
			if ( printState )
			{
				switch (currentGameState)
				{
					case GAME_STATES.MAIN_MENU:
							Debug.Log("Game is currently on main menu");
						break;

					case GAME_STATES.GAMEPLAY:
							Debug.Log("Game is currently on Gameplay");
						break;

					case GAME_STATES.CONSOLE:
							Debug.Log("Game is currently on console mode");
						break;

					case GAME_STATES.PAUSE_MENU:
							Debug.Log("Game is currently on pause menu");
						break;

					default:
						Debug.Log("State cannot be undefined.");
						break;
				}
            
			}
			return currentGameState;
		}

		public static void switchState( GAME_STATES newState )
		{
			if (currentGameState == newState)
			{
				Debug.Log( "cannot switch the state to " + newState + " because it is already in this state." );
				return;
			}
			#if PONG_DEBUG_MODE
				Debug.Log( "CoreStateMachine.cs Attempting to load new scene" );
			#endif
			switch (newState)
			{
				case GAME_STATES.MAIN_MENU:
					// TODO: load main menu here
					sceneLoader.loadScene( SceneLoader.SCENE_POSITION.MAIN_MENU );
				break;

				case GAME_STATES.GAMEPLAY:
					// TODO: load main gameplay here
					sceneLoader.loadScene( SceneLoader.SCENE_POSITION.ARENA );
				break;

				case GAME_STATES.CONSOLE:
					// TODO: load console here
					sceneLoader.loadScene( SceneLoader.SCENE_POSITION.MAIN_MENU );
				break;

				case GAME_STATES.PAUSE_MENU:
					// TODO: load pause menu here
					sceneLoader.loadScene( SceneLoader.SCENE_POSITION.MAIN_MENU );
				break;

				default:
					Debug.Log("State cannot be undefined.");
				break;
			}
		}
	}
}