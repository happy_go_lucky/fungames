﻿using UnityEngine;

namespace Core
{
	public struct Range
	{
		private float maxRange;
		private float minRange;

		public Range( float min = 0, float max = 0 )
		{
			#if PONG_DEBUG_MODE
				Debug.Log( "Arena.cs" );
			#endif

			maxRange = max;
			minRange = min;
		}

		public float max()
		{
			return maxRange;
		}
		public float min()
		{
			return minRange;
		}
	}
}