﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/***
 * This file is responsible for defining the control scheme for the Pong3D game.
 * 
 * */
namespace Core
{
	public static class CaptureUserInput
	{

	#if UNITY_ANDROID

	#endif

	#if UNITY_PS4

	#endif
		// these delegates must be set by objects/scenes expecting user input
		public delegate void ActionButtonClick_Handler();
		public delegate void BackButtonClick_Handler();
		public delegate void RightButtonClick_Handler();
		public delegate void LeftButtonClick_Handler();
		public delegate void UpButtonClick_Handler();
		public delegate void DownButtonClick_Handler();

		public delegate void ActionButtonPress_Handler();
		public delegate void BackButtonPress_Handler();
		public delegate void RightButtonPress_Handler();
		public delegate void LeftButtonPress_Handler();
		public delegate void UpButtonPress_Handler();
		public delegate void DownButtonPress_Handler();

		private static ActionButtonClick_Handler onActionClick = null;
		private static BackButtonClick_Handler onBackClick = null;
		private static RightButtonClick_Handler onRightClick = null;
		private static LeftButtonClick_Handler onLeftClick = null;
		private static UpButtonClick_Handler onUpClick = null;
		private static DownButtonClick_Handler onDownClick = null;

		private static ActionButtonPress_Handler onActionPress = null;
		private static BackButtonPress_Handler onBackPress = null;
		private static RightButtonPress_Handler onRightPress = null;
		private static LeftButtonPress_Handler onLeftPress = null;
		private static UpButtonPress_Handler onUpPress = null;
		private static DownButtonPress_Handler onDownPress = null;
	
		// Update should be called once per frame if you are trying to capture input in your scene.
		public static void updateInput()
		{
			if ( ( Input.GetButton( "Fire1" ) || Input.GetKeyDown( KeyCode.Space ) ) && onActionClick != null )
			{
				Debug.Log("Button clicked = Action");
				onActionClick();
			}
			if ( ( Input.GetAxis( "Horizontal" ) > 0 || Input.GetKeyDown( KeyCode.RightArrow ) ) && onRightClick != null )
			{
				Debug.Log("Button clicked = Right");
				onRightClick();
			}
			if ( ( Input.GetAxis( "Horizontal" ) < 0 || Input.GetKeyDown( KeyCode.LeftArrow ) ) && onLeftClick != null )
			{
				Debug.Log("Button clicked = Left");
				onLeftClick();
			}
			if ( ( Input.GetButton( "Fire1" ) || Input.GetKeyDown( KeyCode.Escape ) ) && onBackClick != null )
			{
				Debug.Log("Button clicked = Back");
				onBackClick();
			}
			if ( ( Input.GetAxis( "Vertical" ) > 0 || Input.GetKeyDown( KeyCode.UpArrow ) ) && onUpClick != null )
			{
				Debug.Log("Button clicked = Up");
				onUpClick();
			}
			if ( ( Input.GetAxis( "Vertical" ) < 0 || Input.GetKeyDown( KeyCode.DownArrow ) ) && onDownClick != null )
			{
				Debug.Log("Button clicked = Down");
				onDownClick();
			}
			// press actions
			if ( Input.GetButton( "Jump" ) || Input.GetKey( KeyCode.Space ) && onActionPress != null )
			{
				Debug.Log("Button pressed = Action");
				onActionPress();
			}
			if ( Input.GetAxis( "Horizontal_R" ) > 0 || Input.GetKey( KeyCode.RightArrow ) && onRightPress != null )
			{
				Debug.Log("Button pressed = Right");
				onRightPress();
			}
			if ( Input.GetAxis( "Horizontal_R" ) < 0 || Input.GetKey( KeyCode.LeftArrow ) && onLeftPress != null )
			{
				Debug.Log("Button pressed = Left");
				onLeftPress();
			}
			if ( Input.GetButton( "Fire1" ) || Input.GetKey( KeyCode.Escape ) && onBackPress != null )
			{
				Debug.Log("Button pressed = Back");
				onBackPress();
			}
			if ( ( Input.GetAxis( "Vertical" ) > 0 || Input.GetKey( KeyCode.UpArrow ) ) && onUpPress != null )
			{
				Debug.Log("Button pressed = Up");
				onUpPress();
			}
			if ( ( Input.GetAxis( "Vertical" ) < 0 || Input.GetKey( KeyCode.DownArrow ) ) && onDownPress != null )
			{
				Debug.Log("Button pressed = Down");
				onDownPress();
			}
		}

		// binds the input to a scene
		public static void attachInput( ActionButtonClick_Handler actionClick = null, BackButtonClick_Handler backClick = null,
										RightButtonClick_Handler rightClick = null, LeftButtonClick_Handler leftClick = null,
										UpButtonClick_Handler upClick = null, DownButtonClick_Handler downClick = null,
										ActionButtonPress_Handler actionPress = null, BackButtonPress_Handler backPress = null,
										RightButtonPress_Handler rightPress = null, LeftButtonPress_Handler leftPress = null,
										UpButtonPress_Handler upPress = null, DownButtonPress_Handler downPress = null )
		{
			onActionClick = actionClick;
			onBackClick = backClick;
			onRightClick = rightClick;
			onLeftClick = leftClick;
			onUpClick = upClick;
			onDownClick = downClick;

			onActionPress = actionPress;
			onBackPress = backPress;
			onRightPress = rightPress;
			onLeftPress = leftPress;
			onUpPress = upPress;
			onDownPress = downPress;
		}

		// removes the input binding
		public static void detachInput()
		{
			onActionClick = null;
			onBackClick = null;
			onRightClick = null;
			onLeftClick = null;
			onUpClick = null;
			onDownClick = null;

			onActionPress = null;
			onBackPress = null;
			onRightPress = null;
			onLeftPress = null;
			onUpPress = null;
			onDownPress = null;
		}
	}
}