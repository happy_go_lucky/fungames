﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/***
 * This script should be attached to the main menu.
 * */
namespace Core.Menus
{
	public class MainMenu : MonoBehaviour, iButtonAction
	{
		private GameObject currentlySelectedButton;
		private int currentButtonIndex;
		private enum BUTTONS { PLAY_BUTTON = 0, INSTRUCTIONS_BUTTON, EXIT_BUTTON };
		private readonly string[] BUTTON_LIST = { "PlayButton", "InstructionsButton", "ExitButton" };

		// Use this for initialization
		void Start()
		{
			#if PONG_DEBUG_MODE
				Debug.Log( "MainMenu.cs" );
			#endif
			
			CaptureUserInput.attachInput( actionClick: onButtonClickAction, backClick: onButtonClickBack,
										  leftClick: onButtonClickLeft, rightClick: onButtonClickRight,
										  downClick: onButtonClickDown, upClick: onButtonClickUp );
			
			selectDefaultButtonInList();
		}
	
		// Update is called once per frame
		void Update()
		{
			CaptureUserInput.updateInput();
		}

		// navigation related Handlers
		public void onButtonPressLeft()
		{
			// no functionality
		}
		public void onButtonPressRight()
		{
			// no functionality
		}
		public void onButtonPressUp()
		{
			
		}
		public void onButtonPressDown()
		{
			
		}
		public void onButtonPressAction()
		{
			// select
			currentlySelectedButton.GetComponent<Button>().Select();
			onPlayButtonSelect();
		}
		public void onButtonPressBack()
		{
			// back
		}

		public void onButtonClickLeft()
		{

		}
		public void onButtonClickRight()
		{

		}
		public void onButtonClickAction()
		{
			if ( currentButtonIndex == (int) BUTTONS.PLAY_BUTTON )
			{
				onPlayButtonSelect();
			}
			else if ( currentButtonIndex == (int) BUTTONS.EXIT_BUTTON )
			{
				onExitButtonSelect();
			}
			else if ( currentButtonIndex == (int) BUTTONS.INSTRUCTIONS_BUTTON )
			{
				// TODO: implement instruction screen and link it.
			}
		}
		public void onButtonClickBack()
		{

		}
		public void onButtonClickUp()
		{
			// move upwards
			updateButtonIndex( true );
		}
		public void onButtonClickDown()
		{
			// move downwards
			updateButtonIndex( false );
		}

		public void onConsoleButtonClick()
		{ }

		private void updateButtonIndex( bool isUpwards )
		{
			if ( isUpwards )
			{
				currentButtonIndex--;
			}
			else
			{
				currentButtonIndex++;
			}
			currentButtonIndex = HelperFunctions.mathModulus( currentButtonIndex, BUTTON_LIST.Length );
			#if PONG_DEBUG_MODE
				Debug.Log( "MainMenu.cs:Button index = " + currentButtonIndex );
			#endif
			currentlySelectedButton = GameObject.Find( BUTTON_LIST[currentButtonIndex] );
			currentlySelectedButton.GetComponent<Button>().Select();
		}

		private void selectDefaultButtonInList()
		{
			currentButtonIndex = ( int ) BUTTONS.PLAY_BUTTON;
			currentlySelectedButton = GameObject.Find( BUTTON_LIST[currentButtonIndex] );
			currentlySelectedButton.GetComponent<Button>().Select();
		}

		private void onPlayButtonSelect()
		{
			// load the game
			#if PONG_DEBUG_MODE
				Debug.Log( "play button is selected" );
			#endif
			changeState( CoreStateMachine.GAME_STATES.GAMEPLAY );
		}

		private void onInstructionsButtonSelect()
		{
			// load instructions
			#if PONG_DEBUG_MODE
				Debug.Log( "instructions button is selected" );
			#endif
		}

		private void onExitButtonSelect()
		{
			// exit the game
			#if PONG_DEBUG_MODE
				Debug.Log( "exit button is selected" );
			#endif

			Application.Quit();
		}

		private void changeState( CoreStateMachine.GAME_STATES state )
		{
			CoreStateMachine.switchState( state );
		}
	}
}