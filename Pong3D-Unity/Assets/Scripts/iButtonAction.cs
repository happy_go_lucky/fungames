﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
	public interface iButtonAction
	{
		// triggers repeatedly until button is held down
		void onButtonPressLeft();
		void onButtonPressRight();
		void onButtonPressAction();
		void onButtonPressBack();
		void onButtonPressUp();
		void onButtonPressDown();
		
		// triggers once until pressed again
		void onButtonClickLeft();
		void onButtonClickRight();
		void onButtonClickAction();
		void onButtonClickBack();
		void onButtonClickUp();
		void onButtonClickDown();

		void onConsoleButtonClick();
	}
}