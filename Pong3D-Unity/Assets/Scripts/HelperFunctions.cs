﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HelperFunctions
{
	public static int mathModulus( int value, int modBy )
	{
		return ( value % modBy + modBy ) % modBy;
	}
}
