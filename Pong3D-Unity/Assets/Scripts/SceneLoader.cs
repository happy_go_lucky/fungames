﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core
{
	public class SceneLoader
	{
		public enum SCENE_POSITION { MAIN_MENU = 0, ARENA, END_GAME, PAUSE_MENU, DUMMY_SCENE };
		public static string[] SCENE_LIST = {	"Assets/Scenes/Menu.unity",
												"Assets/Scenes/Arena.unity"
											};

		private SCENE_POSITION currentScene;
		
		public SceneLoader()
		{
			#if PONG_DEBUG_MODE
				Debug.Log( "SceneLoader.cs" );
			#endif

			currentScene = SCENE_POSITION.DUMMY_SCENE;
		}
	
		// loads a new scene
		public void loadScene( SCENE_POSITION scene )
		{
			// our current design doesn't allow having two scenes loaded at the same time.
			if ( currentScene != SCENE_POSITION.DUMMY_SCENE )
			{
				unloadScene();
			}

			currentScene = scene;
			CaptureUserInput.detachInput();
			#if PONG_DEBUG_MODE
			if ( currentScene != SCENE_POSITION.DUMMY_SCENE )
			{
				Debug.Log( "SceneLoader.cs loading " + SCENE_LIST[(int) currentScene] + " scene" );
			}
			#endif

			SceneManager.LoadSceneAsync( SCENE_LIST[(int) currentScene] );
		}

		public void unloadScene()
		{
			//Scene activeScene = SceneManager.GetActiveScene();

			SceneManager.UnloadSceneAsync( SCENE_LIST[(int) currentScene] );
			currentScene = SCENE_POSITION.DUMMY_SCENE;
		}
	}
}