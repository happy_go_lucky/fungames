﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
	public class RacketActions
	{
		public static readonly int ZERO_ROTATION = 0;
		public static readonly float DISPLACEMENT_RANGE_MIN = -1.8f;
		public static readonly float DISPLACEMENT_RANGE_MAX = 1.8f;

		public static readonly float ROTATION = 1;
		public static readonly float DISPLACEMENT = 0.016f;

		private float currentRotation = 0;
		private float totalRotation = 0;
		private float currentDisplacement = 0;
		private float totalDisplacement = 0;
		private bool switchDir = false;

		private Range rotationRange;
		private Range displacementRange;

		public RacketActions()
		{
			#if PONG_DEBUG_MODE
				Debug.Log( "RacketActions.cs" );
			#endif
			rotationRange = new Range( min: -60, max: 60 );
			displacementRange = new Range( min: DISPLACEMENT_RANGE_MIN, max: DISPLACEMENT_RANGE_MAX );
		}

		public void rotateToLeft()
		{
			currentRotation = 0;
			if ( totalRotation < rotationRange.max() )
			{
				currentRotation += ROTATION;
				totalRotation += currentRotation;
			}
		}

		public void rotateToRight()
		{
			currentRotation = 0;
			if ( totalRotation > rotationRange.min() )
			{
				currentRotation -= ROTATION;
				totalRotation += currentRotation;
			}
			else
			{
				switchDir = !switchDir;
			}
		}
		
		public void rotateToCenter()
		{
			currentRotation = ZERO_ROTATION;
		}

		public void moveDown()
		{
			currentDisplacement = 0;
			if ( totalDisplacement < displacementRange.max() )
			{
				currentDisplacement += DISPLACEMENT;
				totalDisplacement += currentDisplacement;
			}
			else
			{
				switchDir = !switchDir;
			}
		}

		public void moveUp()
		{
			currentDisplacement = 0;
			if ( totalDisplacement > displacementRange.min() )
			{
				currentDisplacement -= DISPLACEMENT;
				totalDisplacement += currentDisplacement;
			}
			else
			{
				switchDir = !switchDir;
			}
		}

		public float getRotation()
		{
			float temp = currentRotation;
			currentRotation = 0;
			return temp;
		}

		public float getDisplacement()
		{
			float temp = currentDisplacement;
			currentDisplacement = 0;
			return temp;
		}

		public bool switchDirection()
		{
			return switchDir;
		}
	}
}