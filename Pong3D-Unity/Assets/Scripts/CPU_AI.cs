﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
	public class CPU_AI : MonoBehaviour
	{
		private GameObject playerRacket;
		private GameObject playerRacketContainer;
		private RacketActions playerRacketActions;

		// Use this for initialization
		void Start ()
		{
			#if PONG_DEBUG_MODE
				Debug.Log( "CPU_AI.cs" );
			#endif
			playerRacket = GameObject.Find( Arena.SCENE_OBJECTS[(int) Arena.SCENE_OBJECT_ORDER.RACKET_BLACK] );
			playerRacketContainer = GameObject.Find( Arena.SCENE_OBJECTS[(int) Arena.SCENE_OBJECT_ORDER.BLACK_RACKET_CONTAINER] );
			playerRacketActions = new RacketActions();
		}
	
		// Update is called once per frame
		void Update ()
		{
			if ( playerRacketActions.switchDirection() )
			{
				playerRacketActions.moveDown();
			}
			else
			{
				playerRacketActions.moveUp();
			}

			playerRacketContainer.transform.Translate( 0, 0, playerRacketActions.getDisplacement() );
			//playerRacket.transform.Rotate( 0, RacketActions.DISPLACEMENT, 0 );
		}
	}
}