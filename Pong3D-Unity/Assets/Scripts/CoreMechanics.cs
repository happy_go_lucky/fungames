﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/***
 * This defines the root of the Pong3D.
 * Ideally it should be attached to an empty scene which is always there in the background.
 * This should not be controlled via user input.
 * */
namespace Core
{
	public class CoreMechanics : MonoBehaviour
	{
		void Start ()
		{
			#if PONG_DEBUG_MODE
				Debug.Log( "CoreMechanics.cs" );
				Debug.Log( "System Version: " + System.Environment.Version );
			#endif

			// load all the core mechanics here
			
			// input responder. Should be loaded before scene loader
			CaptureUserInput.detachInput();

			// state machine
			requestStateChange( CoreStateMachine.GAME_STATES.MAIN_MENU );
			// etc
		}
	
		// Update is called once per frame
		void Update ()
		{
			CoreStateMachine.checkGameState();
		}

		public void requestStateChange( CoreStateMachine.GAME_STATES newState )
		{
			CoreStateMachine.switchState( newState );
		}
	}
}