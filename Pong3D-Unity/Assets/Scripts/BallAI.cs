﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
	public class BallAI : MonoBehaviour
	{
		SphereCollider ballCollider;
		Rigidbody ballRigidbody;

		// Use this for initialization
		void Start ()
		{
			#if PONG_DEBUG_MODE
				Debug.Log( "BallAI.cs" );
			#endif
			
			ballCollider = GetComponent<SphereCollider>();
			ballRigidbody = GetComponent<Rigidbody>();
			ballRigidbody.velocity = new Vector3( -2, 0, 0 );
		}
	
		// Update is called once per frame
		void Update ()
		{
			
		}

		public void setBallVelocity( float velocity )
		{
			ballRigidbody.velocity = new Vector3( velocity, 0, 0 );
		}

		void OnCollisionEnter(Collision collision)
		{
			Debug.Log("collision");
		}
	}
}