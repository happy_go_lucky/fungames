﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Core
{
	public class Arena : MonoBehaviour, iButtonAction
	{
		public enum SCENE_OBJECT_ORDER { RACKET_RED = 0, RACKET_BLACK, BALL, RED_RACKET_CONTAINER, BLACK_RACKET_CONTAINER };
		public static readonly string[] SCENE_OBJECTS = { "RacketRed", "RacketBlack", "Ball", "RedRacketContainer", "BlackRacketContainer" };

		private RacketActions playerRacketActions;
		private GameObject playerRacket;
		private GameObject playerRacketContainer;
		private GameObject ball;
		private	Text playerScoreDisplay;
		private	Text cpuScoreDisplay;

		private readonly Vector3 CPU_BOUNDS = new Vector3( 5, 0, 0 );
		private readonly Vector3 PLAYER_BOUNDS = new Vector3( -5, 0, 0 );
		private readonly uint MAX_SCORE = 5;
		private readonly float BALL_Y = 2.2f;

		private uint playerScore = 0;
		private uint cpuScore = 0;
		
		void Start ()
		{
			#if PONG_DEBUG_MODE
				Debug.Log( "Arena.cs" );
			#endif
			CaptureUserInput.attachInput( actionClick: onButtonClickAction, backClick: onButtonClickBack,
										  leftPress: onButtonPressLeft, rightPress: onButtonPressRight,
										  downPress: onButtonPressDown, upPress: onButtonPressUp );

			playerRacketActions = new RacketActions();
			playerRacket = GameObject.Find( SCENE_OBJECTS[(int) SCENE_OBJECT_ORDER.RACKET_RED] );
			playerRacketContainer = GameObject.Find( SCENE_OBJECTS[(int) SCENE_OBJECT_ORDER.RED_RACKET_CONTAINER] );
			ball = GameObject.Find( SCENE_OBJECTS[(int) SCENE_OBJECT_ORDER.BALL] );
			//ball.transform.Translate( 0, 2.2f, 0 );

			playerScoreDisplay = GameObject.Find("PlayerScore").GetComponent<Text>();
			cpuScoreDisplay = GameObject.Find("CPUScore").GetComponent<Text>();
		}
	
		// Update is called once per frame
		void Update ()
		{
			CaptureUserInput.updateInput();
			playerRacket.transform.Rotate( 0, playerRacketActions.getRotation(), 0 );
			playerRacketContainer.transform.Translate( playerRacketActions.getDisplacement(), 0, 0 );

			if ( ball.transform.position.x > CPU_BOUNDS.x )
			{
				// player wins
				stopBall();
				playerScore++;
				resetBall();
			}

			if ( ball.transform.position.x < PLAYER_BOUNDS.x )
			{
				// cpu wins
				stopBall();
				cpuScore++;
				resetBall();
			}

			if ( cpuScore == MAX_SCORE )
			{
				// cpu wins
				resetBall();
				resetScore();

			}
			if ( playerScore == MAX_SCORE )
			{
				// player wins
				resetBall();
				resetScore();
			}

			updateScore();
		}

		public void onButtonPressLeft()
		{
			// turn the racket
			playerRacketActions.rotateToLeft();
		}
		public void onButtonPressRight()
		{
			// turn the racket
			playerRacketActions.rotateToRight();
		}
		public void onButtonPressAction()
		{
			// enter command
		}
		public void onButtonPressBack()
		{
			// exit scene or console
		}
		public void onButtonPressUp()
		{
			// move the racket up
			playerRacketActions.moveUp();
		}
		public void onButtonPressDown()
		{
			// turn the racket down
			playerRacketActions.moveDown();
		}
		
		public void onButtonClickLeft()
		{ }
		public void onButtonClickRight()
		{ }
		public void onButtonClickAction()
		{ }
		public void onButtonClickBack()
		{ }
		public void onButtonClickUp()
		{ }
		public void onButtonClickDown()
		{ }

		public void onConsoleButtonClick()
		{
			// TODO: bringup a console
		}

		private void resetBall()
		{
			ball.transform.position = new Vector3( 0, BALL_Y, 0 );
			ball.GetComponent<BallAI>().setBallVelocity( -2 );
		}

		private void resetScore()
		{
			playerScore = 0;
			cpuScore = 0;
			updateScore();
		}

		private void updateScore()
		{
			playerScoreDisplay.text = playerScore.ToString();
			cpuScoreDisplay.text = cpuScore.ToString();
		}

		private void stopBall()
		{
			ball.GetComponent<BallAI>().setBallVelocity( 0 );
		}

		private void endGame()
		{
			CoreStateMachine.switchState( CoreStateMachine.GAME_STATES.MAIN_MENU );
		}
	}
}