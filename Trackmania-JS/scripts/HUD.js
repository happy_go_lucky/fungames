function HUD(ctx, x, y, width, height) { 
	const NULL_STR = "";
	const SPEEDO_PATH = "images/speedo.png";
	const TIMER_PATH = "images/timer.png";
	const RPMS_PATH = "images/rpms.png";
	const SHIFT_PATH = "images/shiftknob.png";
	
	const ORANGE_COLOR = "#FF6600";
	const BLUE_COLOR = "#AAFFEE";
	const RED_COLOR = "#800000";
	
	const ICON_SIZE = 80;
	const ICON_PADDING = 5;
	const SPEEDO_X = ((width - ICON_SIZE) / 2) - 100;
	const SHIFT_X = ((width - ICON_SIZE) / 2) + 100;
	const RPMS_X = 10;
	const TIMER_X = width - ICON_SIZE - 10;
	const TEXT_Y = 60;
	const TEXT_GAP = 10;
	
	var speedo = new Image();
	var timer = new Image();
	var rpms = new Image();
	var shift = new Image();
	
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	ctx.fillStyle = BLUE_COLOR;
	ctx.font = "40px Arial";
	
	this.clearHUD = function() {
		this.update(NULL_STR, NULL_STR, NULL_STR, NULL_STR, NULL_STR, NULL_STR);
	};
	
	this.update = function (speed, gear, rpms, time, test) {
		ctx.clearRect(0, 0, width, height);
		
		ctx.fillText(speed, SPEEDO_X + ICON_SIZE + TEXT_GAP, TEXT_Y);
		ctx.fillText(gear, SHIFT_X + ICON_SIZE + TEXT_GAP, TEXT_Y);
		ctx.fillText(rpms, RPMS_X + ICON_SIZE + TEXT_GAP, TEXT_Y);
		ctx.fillText(time, TIMER_X - 120, TEXT_Y);
		
		drawSpeedo();
		drawTimer();
		drawRPMS();
		drawShift();
		
		// ctx.fillText("test: " + test, 400, 750);
	};
	
	function loadHUD() {
		loadSpeedo();
		loadTimer();
		loadRPMS();
		loadShift();
	}
	
	function loadSpeedo() {
		speed = new Image();
		speedo.onload = drawSpeedo;
		speedo.src = SPEEDO_PATH;
	}
	
	function loadRPMS() {
		rpms = new Image();
		rpms.onload = drawRPMS;
		rpms.src = RPMS_PATH;
	}
	
	function loadTimer() {
		timer = new Image();
		timer.onload = drawTimer;
		timer.src = TIMER_PATH;
	}
	
	function loadShift() {
		shift = new Image();
		shift.onload = drawShift;
		shift.src = SHIFT_PATH;
	}
	
	function drawSpeedo() {
		ctx.drawImage(speedo, SPEEDO_X, ICON_PADDING, ICON_SIZE, ICON_SIZE);
	}
	
	function drawRPMS() {
		ctx.drawImage(rpms, RPMS_X, ICON_PADDING, ICON_SIZE, ICON_SIZE);
	}
	
	function drawTimer() {
		ctx.drawImage(timer, TIMER_X, ICON_PADDING, ICON_SIZE, ICON_SIZE);
	}
	
	function drawShift() {
		ctx.drawImage(shift, SHIFT_X, ICON_PADDING, ICON_SIZE, ICON_SIZE);
	}
	
	loadHUD();
}