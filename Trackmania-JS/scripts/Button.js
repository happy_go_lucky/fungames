function Button(ctx, x, y, width, height, fontFamily, textsize, textColor, textWeight, text, command) {
	const IMAGE_PATH = "images/";
	const IMG_EXTENSION = ".png";
	const BUTTON_IMG = "button";
	
	var scaleProportion = 0;
	var btn_x = 0;
	var btn_y = 0;
	var btn_width = 0;
	var btn_height = 0;
	var button = null;
	var button_drawn = false;
	
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	this.fontFamily = fontFamily;
	this.textsize = textsize;
	this.text = text;
	this.command = command;
	this.remove = function() {
		clearButton();
	};
	
	this.onMouseMove = function(e) {
		if ( (e.pageX >= x && e.pageX <= x + width) && (e.pageY >= y && e.pageY <= y + height)) {
			if (scaleProportion != 30 / 100) {
				if (button_drawn) {
					clearButton();
				}
				scaleProportion = 30 / 100;
				createButton(x, y, width, height, text, scaleProportion);
			}
		}
		else {
			if (scaleProportion != 0) {
				if (button_drawn) {
					clearButton();
				}
				scaleProportion = 0;
				createButton(x, y, width, height, text, scaleProportion);
			}
		}
	};
	
	this.onMouseDown = function(e) {
		if ( (e.pageX >= x && e.pageX <= x + width) && (e.pageY >= y && e.pageY <= y + height)) {
			clearButton();
			scaleProportion = 0;
			createButton(x, y, width, height, text, scaleProportion);
		}
	};
	
	this.onMouseUp = function(e) {
		if ( (e.pageX >= x && e.pageX <= x + width) && (e.pageY >= y && e.pageY <= y + height)) {
			clearButton();
			scaleProportion = 30 / 100;
			createButton(x, y, width, height, text, scaleProportion);
			return true;
		}
		return false;
	};
	
	function createButton(x, y, width, height, text, scale) {
		ctx.font = textWeight + " " + textsize + "px " + fontFamily;
		ctx.fillStyle = textColor;
		
		btn_x = x;
		btn_y = y;
		btn_width = width;
		btn_height = height;
		
		btn_width = Math.floor(btn_width + (btn_width * scale));
		btn_height = Math.floor(btn_height + (btn_height * scale));
		btn_x = Math.floor(btn_x - (btn_width * scale) / 2);
		btn_y = Math.floor(btn_y - (btn_height * scale) / 2);
		
		button = new Image();
		button.onload = drawButton;
		button.src = IMAGE_PATH + BUTTON_IMG + IMG_EXTENSION;
	}
	
	function drawButton() {
		button_drawn = true;
		ctx.drawImage(button, btn_x, btn_y, btn_width, btn_height);
		
		var font_x = ((btn_width - ctx.measureText(text).width) / 2);
		var font_y = ((btn_height - textsize) / 2) + textsize;
		ctx.fillText(text, btn_x + font_x, btn_y + font_y);
	}
	
	function clearButton() {
		button_drawn = false;
		button.onload = null;
		ctx.clearRect(btn_x, btn_y, btn_width, btn_height);
	}
	
	createButton(x, y, width, height, text, scaleProportion);
}