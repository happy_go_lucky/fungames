function MenuCommands() {
	const START = "start";
	const CREDITS = "credits";
	const HELP = "help";
	const NULL = "nulled";
	const BACK = "go_back";
	const FINISHED_RACE = "finished_race";
	const NEXT_TRACK = "next_track";
	const END_GAME = "end_game";
	
	this.start_cmd = START;
	this.credits_cmd = CREDITS;
	this.help_cmd = HELP;
	this.back_cmd = BACK;
	this.finish_race_cmd = FINISHED_RACE;
	this.next_track_cmd = NEXT_TRACK;
	this.end_game = END_GAME;
	this.null_cmd = NULL;
}