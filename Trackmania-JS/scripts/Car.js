console.log("car");

function Car(ctx, x, y, width, height, canvas_W, canvas_H) { 
	
	const CAR_IMAGE_SRC = "images/car.svg";
	
	const KEY_LEFT = 37;
	const KEY_UP = 38;
	const KEY_RIGHT = 39;
	const KEY_DOWN = 40;
	
	const KEY_Z = 90;
	const KEY_X = 88;
	
	const ACCELERATION_BASE = 80;
	const TOP_SPEED = 100;
	const REVERSE_SPEED = 50;
	const REFRESH_RATE = Math.floor(1000 / 30);
	const MAX_ENGINE_RPMS = 5000;
	
	const TURN_ANGLE = 4;
	const MAX_TURN_ANGLE = 5 * TURN_ANGLE;
	
	const GEAR_1_MAX_SPEED = Math.floor(TOP_SPEED / 5);
	const GEAR_2_MAX_SPEED = Math.floor(TOP_SPEED / 2.5);
	const GEAR_3_MAX_SPEED = Math.floor(TOP_SPEED / 1.5);
	const GEAR_4_MAX_SPEED = TOP_SPEED;
	
	const KMS_TO_PIXEL_RATIO = 7000;
	
	const RPM_REV_DOWN = 600;
	const RPM_BRAKE_REV_DOWN = 100;
	
	const TRANSMISSION = { GEAR_R: 'R', GEAR_N: 'N', GEAR_1: '1', GEAR_2: '2', GEAR_3: '3', GEAR_4: '4' };
	
	var car_img = new Image();
	var speed = 0.0;
	var acceleration = 0.0;
	var turnAngle = 0.0;
	var traction;
	var isRunnable = false;
	var canvas_width = canvas_W;
	var canvas_height = canvas_H;
	var isTurnable = true;
	var isBraking = true;
	var isNeutral = false;
	var currentGear = TRANSMISSION.GEAR_N;
	var engineRevs = 0.0;
	var gear_engine_ratio;
	var max_speed_in_current_gear = 0;
	
	var updateInterval = null;
	var accelerateInterval = null;
	var deaccelerateInterval = null;
	var carCoords = { coord_x: x, coord_y: y}
	var totalTurnAngle = 0.0;
	
	this.cx = x;
	this.cy = y;
	this.width = canvas_width;
	this.height = canvas_height;
	
	ctx.save();
	car_img.src = CAR_IMAGE_SRC;
	
	this.getCurrentSpeed = function() {
		return Math.abs(Math.floor(speed));
	};
	
	this.saveState = function() {
		// ctx.save();
	}
	
	this.resetState = function() {
		ctx.restore();
		deaccelerateInterval = _checkClearInterval(deaccelerateInterval);
		accelerateInterval = _checkClearInterval(accelerateInterval);
		updateInterval = _checkClearInterval(updateInterval);
	}
	
	this.getCurrentGear = function() {
		return currentGear;
	}
	
	this.getCurrentRpms = function() {
		return Math.abs(Math.floor(engineRevs));
	}
	
	this.getPos = function() {
		return carCoords;
	}
	
	this.testGetAngle = function () {
		return totalTurnAngle;
	}
	
	this.move = function(direction) {
		_handleKeyDown(direction);
	}
	
	this.stopMove = function(direction) {
		_handleKeyUp(direction);
	}
	
	this.isRunnable = function() {
		return isRunnable;
	}
	
	car_img.onload = function () {
		_drawCar();
		updateInterval = setInterval(_roll, REFRESH_RATE);
	};
	
	function _drawCar() {
		ctx.clearRect(0 - width / 2, 0 - height / 2, canvas_W, canvas_H);
		
		if (_isMoving()) {
			totalTurnAngle += +turnAngle;
			carCoords.coord_x += x * Math.cos(totalTurnAngle * (Math.PI / 180));
			carCoords.coord_y += x * Math.sin(totalTurnAngle * (Math.PI / 180));
		}
		
		ctx.translate(x, y);
		x = y = 0;
		ctx.rotate(turnAngle * (Math.PI / 180));
		turnAngle = 0;
		ctx.drawImage(car_img, 0 - width / 2, 0 - height / 2, width, height);
	}
	
	function _handleKeyDown(event) {
		if (event.keyCode == KEY_LEFT) {
			_turn(KEY_LEFT);
		}
		else if (event.keyCode == KEY_UP) {
			deaccelerateInterval = _checkClearInterval(deaccelerateInterval);
			if (accelerateInterval == null) {
				console.log("created accelerateInterval");
				accelerateInterval = setInterval(_accellerate, REFRESH_RATE);
			}
		}
		else if (event.keyCode == KEY_RIGHT) {
			_turn(KEY_RIGHT);
		}
		else if (event.keyCode == KEY_DOWN) {
			isBraking = true;
		}
	}
	
	function _handleKeyUp(event) {
		if (event.keyCode == KEY_LEFT) {
			turnAngle = 0;
			isTurnable = true;
		}
		else if (event.keyCode == KEY_UP) {
			accelerateInterval = _checkClearInterval(accelerateInterval);
			if (deaccelerateInterval == null) {
				deaccelerateInterval = setInterval(revDownEngine, REFRESH_RATE);
			}
		}
		else if (event.keyCode == KEY_RIGHT) {
			turnAngle = 0;
			isTurnable = true;
		}
		else if (event.keyCode == KEY_DOWN) {
			isBraking = false;
		}
		else if (event.keyCode == KEY_Z) {
			if (shiftDownGears()) {
				loadBalanceEngine();
			}
		}
		else if (event.keyCode == KEY_X) {
			if (shiftUpGears()) {
				loadBalanceEngine();
			}
		}
	}
	
	function _accellerate() {
		revUpEngineManual();
	}
	
	function _roll() {
		// console.log("engine rpms: " + engineRevs);
		// console.log("speed kmph: " + getSpeed()/*speed*/);
		// console.log("current gear: " + currentGear);
		_drawCar();
		_moveCar();
	}
	
	function _turn(direction) {
		if (direction == KEY_LEFT && _isMoving()) {
			turnAngle -= +TURN_ANGLE;
		}
		else if (direction == KEY_RIGHT && _isMoving()) {
			turnAngle += +TURN_ANGLE;
		}
		else {
			turnAngle = 0;
		}
	}
	
	function _checkClearInterval(intervalInstance) {
		console.log("clearing interval");
		if (intervalInstance != null) {
			clearInterval(intervalInstance);
		}
		
		return null;
	}
	
	function _moveCar() {
		// time in ms
		speed = getSpeed();
		x += ((speed / 3600000) * REFRESH_RATE) * KMS_TO_PIXEL_RATIO;
		// console.log("x " + x);
	}
	
	function shiftUpGears() {
		if (currentGear == TRANSMISSION.GEAR_R) {
			currentGear = TRANSMISSION.GEAR_N;
			max_speed_in_current_gear = 0;
			return true;
		}
		else if (currentGear == TRANSMISSION.GEAR_N) {
			currentGear = TRANSMISSION.GEAR_1;
			max_speed_in_current_gear = GEAR_1_MAX_SPEED;
			return true;
		}
		else if (currentGear == TRANSMISSION.GEAR_1) {
			currentGear = TRANSMISSION.GEAR_2;
			max_speed_in_current_gear = GEAR_2_MAX_SPEED;
			return true;
		}
		else if (currentGear == TRANSMISSION.GEAR_2) {
			currentGear = TRANSMISSION.GEAR_3;
			max_speed_in_current_gear = GEAR_3_MAX_SPEED;
			return true;
		}
		else if (currentGear == TRANSMISSION.GEAR_3) {
			currentGear = TRANSMISSION.GEAR_4;
			max_speed_in_current_gear = GEAR_4_MAX_SPEED;
			return true;
		}
		
		return false;
	}
	
	function shiftDownGears() {
		if (currentGear == TRANSMISSION.GEAR_4) {
			currentGear = TRANSMISSION.GEAR_3;
			max_speed_in_current_gear = GEAR_3_MAX_SPEED;
			return true;
		}
		else if (currentGear == TRANSMISSION.GEAR_3) {
			currentGear = TRANSMISSION.GEAR_2;
			max_speed_in_current_gear = GEAR_2_MAX_SPEED;
			return true;
		}
		else if (currentGear == TRANSMISSION.GEAR_2) {
			currentGear = TRANSMISSION.GEAR_1;
			max_speed_in_current_gear = GEAR_1_MAX_SPEED;
			return true;
		}
		else if (currentGear == TRANSMISSION.GEAR_1) {
			currentGear = TRANSMISSION.GEAR_N;
			max_speed_in_current_gear = 0;
			return true;
		}
		else if (currentGear == TRANSMISSION.GEAR_N) {
			currentGear = TRANSMISSION.GEAR_R;
			max_speed_in_current_gear = GEAR_1_MAX_SPEED;
			return true;
		}
		
		return false;
	}
	
	function _getAcceleration() {
		if (currentGear == TRANSMISSION.GEAR_R) {
			return -ACCELERATION_BASE;
		}
		else if (currentGear == TRANSMISSION.GEAR_N) {
			return MAX_ENGINE_RPMS / 100;
		}
		else if (currentGear == TRANSMISSION.GEAR_1) {
			return ACCELERATION_BASE;
		}
		else if (currentGear == TRANSMISSION.GEAR_2) {
			return ACCELERATION_BASE / 3;
		}
		else if (currentGear == TRANSMISSION.GEAR_3) {
			return ACCELERATION_BASE / 6;
		}
		else {
			return ACCELERATION_BASE / 10;
		}
	}
	
	function _isMoving() {
		return Math.floor(Math.abs(speed)) != 0;
	}
	
	function revDownEngine() {
		// console.log("revDownEngine " + isBraking);
		if (engineRevs > 0 && currentGear != TRANSMISSION.GEAR_R) {
			engineRevs -= MAX_ENGINE_RPMS / (isBraking ? RPM_BRAKE_REV_DOWN : RPM_REV_DOWN);
		}
		else if (engineRevs < 0 && currentGear == TRANSMISSION.GEAR_R) {
			engineRevs += MAX_ENGINE_RPMS / (isBraking ? RPM_BRAKE_REV_DOWN : RPM_REV_DOWN);
		}
		else {
			engineRevs = 0;
			deaccelerateInterval = _checkClearInterval(deaccelerateInterval);
		}
	}
	
	function revUpEngineManual() {
		if (Math.abs(engineRevs) < MAX_ENGINE_RPMS) {
			engineRevs += _getAcceleration();
		}
	}
	
	function loadBalanceEngine() {
		engineRevs -= engineRevs / 3;
	}
	
	function getSpeed() {
		return Math.floor(max_speed_in_current_gear * (engineRevs / MAX_ENGINE_RPMS));
	}
	
	function updateTurnAngle() {
		if (totalTurnAngle >= 360) {
			return totalTurnAngle - 360;
		}
		else if (totalTurnAngle <= 0) {
			return totalTurnAngle + 360;
		}
		
		return totalTurnAngle;
	}
}