console.log("TrackCheckpoints");

function TrackCheckpoints(ctx, x, y, width, height, blockSize, spawnIndices) {

	console.log("TrackCheckpoints created");
	
	const TRACK_IMAGE = "images/track_";
	const IMAGE_TYPE = ".png";
	const IMAGE_CHECK_PT = "_cp";
	const IMAGE_CHECK_ACTIVE = "_active";
	
	const GRASS = "water";
	const CLEAR = "invisible";
	const HORIZ_CP = "straight_H_cp";
	const VERT_CP = "straight_V_cp";
	const TL_CP = "curve_TL_cp";
	const TR_CP = "curve_TR_cp";
	const BL_CP = "curve_BL_cp";
	const BR_CP = "curve_BR_cp";
	
	var trackData = new TrackCPDataDef();
	var trackArray = new Array();
	var checkPointArray = new Array();
	var numRows = height / blockSize;
	var numCols = width / blockSize;
	var currentTrackNumber = 0;
	var currentTrack = trackData.TRACK_ORDER[0];
	var totalCheckpoints = 0;
	var capturedCheckPoints = 0;
	var regexpPattern = /_cp/i;
	var imageLoadCounter = 0;
	var menuCommands = new MenuCommands();
	
	this.x = x;
	this.y = y;
	this.height = height;
	this.width = width;
	this.blockSize = blockSize;
	this.registerCheckPoints = function(blockIndices) {
		if ( checkPointArray.length > 0 ) {
			if ( (blockIndices.i == checkPointArray[0].row) && (blockIndices.j == checkPointArray[0].col) ) {
				checkPointArray = arrayRemoveItemAt(checkPointArray, 0);
				trackArray[+blockIndices.i][+blockIndices.j].onload = drawCheckpointsOnCanvas;
				trackArray[+blockIndices.i][+blockIndices.j].src = TRACK_IMAGE + CLEAR + IMAGE_TYPE;
				
				highlightNextCheckPoint();
			}
			return menuCommands.null_cmd;
		}
		else {
			console.log("winner");
			return menuCommands.finish_race_cmd;
		}
	};
	
	this.resetCheckPoints = function() {
		trackArray = new Array();
		checkPointArray = new Array()
	};
	
	this.loadCheckPoints = function(number) {
		// console.log("checkpoint track: ", number);
		currentTrackNumber = number;
		currentTrack = trackData.TRACK_ORDER[number].track_layout;
		this.resetCheckPoints();
		loadOrderedCheckpoints();
		loadTrack();
	};
	
	function loadOrderedCheckpoints() {
		checkPointArray = copyCPArray(trackData.TRACK_ORDER[currentTrackNumber].cp_order, checkPointArray);
		totalCheckpoints = checkPointArray.length;
	}
	
	function loadTrack() {
		for (var ii = 0; ii < numRows; ii++) {
			trackArray.push(new Array());
			for (var jj = 0; jj < numCols; jj++) {
				trackArray[ii].push(new Image());
				trackArray[ii][jj].onload = onCheckPointsLoaded;
				trackArray[ii][jj].src = TRACK_IMAGE + currentTrack[ii][jj] + IMAGE_TYPE;
			}
		}
	}
	
	function onCheckPointsLoaded() {
		imageLoadCounter++;
		if (imageLoadCounter == numRows * numCols) {
			imageLoadCounter = 0;
			drawCheckpointsOnCanvas();
			highlightNextCheckPoint();
		}
	}
	
	function drawCheckpointsOnCanvas() {
		ctx.clearRect(0, 0, width, height);
		for (var ii = 0; ii < numRows; ii++) {
			for (var jj = 0; jj < numCols; jj++) {
				ctx.drawImage(trackArray[ii][jj], jj * blockSize, ii * blockSize, blockSize, blockSize);
			}
		}
	}
	
	function highlightNextCheckPoint() {
		if ( checkPointArray.length > 0 ) {
			trackArray[+checkPointArray[0].row][+checkPointArray[0].col].onload = drawCheckpointsOnCanvas;
			trackArray[+checkPointArray[0].row][+checkPointArray[0].col].src = TRACK_IMAGE + currentTrack[+checkPointArray[0].row][+checkPointArray[0].col] + IMAGE_CHECK_ACTIVE + IMAGE_TYPE;
		}
	}
	
	function arrayRemoveItemAt(arr, index) {
		var newArr = new Array();
		
		for (var ii = 0; ii < arr.length; ii++) {
			if (ii != index) {
				newArr.push(arr[ii]);
			}
		}
		
		return newArr;
	}
	
	function copyCPArray(src, dest) {
		if (dest.length != src.length) {
			dest = new Array(src.length);
		}
		
		for (var ii = 0; ii < src.length; ii++) {
			dest[ii] = { row: src[ii].row, col: src[ii].col };
		}
		
		return dest;
	}
}