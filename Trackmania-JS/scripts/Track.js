console.log("Track");

function Track(ctx, height, width, blockSize) {

	console.log("Track created");
	
	const TRACK_IMAGE = "images/track_";
	const IMAGE_TYPE = ".png";
	
	const GRASS = "grass";
	const WATER = "water";
	const HORIZ = "straight_H";
	const VERT = "straight_V";
	const TL = "curve_TL";
	const TR = "curve_TR";
	const BL = "curve_BL";
	const BR = "curve_BR";
	
	var trakData = new TrackDataDef(blockSize);
	var trackArray = new Array();
	var numRows = height / blockSize;
	var numCols = width / blockSize;
	// var currentTrackNumber = -1;
	var currentTrackNumber = -1;
	var currTrackSpawnPoint = -1;
	var currentTrack = -1;
	var imageLoadCounter = 0;
	
	this.height = height;
	this.width = width;
	this.blockSize = blockSize;
	
	this.spawnPoint = function() {
		return currTrackSpawnPoint;
	};
	
	this.getCurrentBlock = currentBlock;
	this.isOnTrack = onTrack;
	
	this.loadNextTrack = function() {
		if (currentTrackNumber < trakData.TRACK_ORDER.length - 1) {
			currentTrackNumber++;
			loadTrack(currentTrackNumber);
		}
		else {
			console.log("finish");
		}
	};
	
	this.isLastTrack = function() {
		return currentTrackNumber >= trakData.TRACK_ORDER.length - 1;
	};
	
	this.getCurrentTrack = function() {
		return currentTrackNumber;
	};
	
	this.clearTrack = function() {
		ctx.clearRect(0, 0, width, height);
	};
	
	function loadTrackData() {
		
		for (var ii = 0; ii < numCols; ii++) {
			trackArray.push(new Array());
			for (var jj = 0; jj < numRows; jj++) {
				trackArray[ii].push(new Image());
				trackArray[ii][jj].onload = onTrackImageLoad;
				trackArray[ii][jj].src = TRACK_IMAGE + currentTrack[ii][jj] + IMAGE_TYPE;
			}
		}
	}
	
	function onTrackImageLoad() {
		imageLoadCounter++;
		if (imageLoadCounter == numRows * numCols) {
			imageLoadCounter = 0;
			drawTrackOnCanvas();
		}
	}
	
	function drawTrackOnCanvas() {
		for (var ii = 0; ii < numCols; ii++) {
			for (var jj = 0; jj < numRows; jj++) {
				ctx.drawImage(trackArray[ii][jj], jj * blockSize, ii * blockSize, blockSize, blockSize);
			}
		}
	}
	
	function currentBlock(position) {
		return { i: Math.floor(position.coord_y / blockSize), j: Math.floor(position.coord_x / blockSize) };
	}
	
	function onTrack(posBlock) {
		// console.log("onTrack: ", posBlock, currentTrack);
		if (currentTrack[posBlock.i][posBlock.j] == GRASS)
		{
			return false;
		}
		
		return true;
	}
	
	function loadTrack(trackNumber) {
		// console.log("track Loaded", currentTrackNumber, TRACK_ORDER[currentTrackNumber].spawn_coord);
		currTrackSpawnPoint = trakData.TRACK_ORDER[currentTrackNumber].spawn_coord;
		currentTrack = trakData.TRACK_ORDER[currentTrackNumber].track;
		loadTrackData();
	}
}