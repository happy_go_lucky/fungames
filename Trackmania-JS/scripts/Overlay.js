function Overlay(ctx, x, y, width, height) {
	console.log("overlay created");
	const IMAGE_PATH = "images/";
	const OVERLAY_IMG = "overlay";
	const IMG_EXTENSION = ".png";
	const START = "title";
	const MENU = "menu_bg";
	const FINISH_IMG = "finish";
	
	var overlay = new Image();
	
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	this.remove = function() {
		clearCanvas();
	};
	
	this.loadStart = function() {
		clearCanvas();
		overlay.src = IMAGE_PATH + START + IMG_EXTENSION;
	};
	
	this.loadOverlay = function() {
		clearCanvas();
		overlay.src = IMAGE_PATH + OVERLAY_IMG + IMG_EXTENSION;
	};
	
	this.loadMenuBG = function() {
		clearCanvas();
		overlay.src = IMAGE_PATH + MENU + IMG_EXTENSION;
	};
	
	this.loadEndGame = function() {
		clearCanvas();
		overlay.src = IMAGE_PATH + FINISH_IMG + IMG_EXTENSION;
	};
	
	function loadOverlayImage() {
		overlay.onload = drawImageOnCanvas;
		overlay.src = IMAGE_PATH + OVERLAY_IMG + IMG_EXTENSION;
	}
	
	function drawImageOnCanvas() {
		ctx.drawImage(overlay, x, y, width, height);
	}
	
	function clearCanvas() {
		ctx.clearRect(0, 0, width, height);
	}
	
	loadOverlayImage();
}