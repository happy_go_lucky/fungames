function Menu(ctx, x, y, width, height) {
	console.log("menu created");
	
	const IMAGE_PATH = "images/";
	const HELP_IMAGE = "help";
	const IMAGE_TYPE = ".png";
	
	const MESSAGE_BG = "hud_bg";
	
	const CREDITS_FONT_HEIGHT = 30;
	const BUTTON_FONT_HEIGHT = 20;
	const BUTTON_FONT_TYPE = "Ariel";
	const BUTTON_FONT_WEIGHT = "bold";
	const MESSAGE_FONT = "60px Ariel bold";
	
	const BUTTON_WIDTH = 200;
	const BUTTON_HEIGHT = 60;
	
	const BUTTON_FONT_COLOR = "white";
	const ORANGE_COLOR = "#FF6600";
	const RED_COLOR = "#800000";
	
	const SUCCESS_MESSAGES= ["Good Job Ricer!", "Sweet!", "Great Job!", "Nailed It!", "Yay!!"];
	
	var buttonArray = new Array();
	var menuCommands = new MenuCommands();
	var helpImage = new Image();
	var messageImg = new Image();
	var finishImg = new Image();
	
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	this.remove = function() {
		if (buttonArray.length > 0) {
			for(var ii = 0; ii < buttonArray.length; ii++) {
				buttonArray[ii].remove();
			}
		}
		
		buttonArray = new Array();
		clearCanvas();
	};
	
	this.onMouseMove = function(e) {
		if (buttonArray.length > 0) {
			for(var ii = 0; ii < buttonArray.length; ii++) {
				buttonArray[ii].onMouseMove(e);
			}
		}
	}
	
	this.onMouseDown = function(e) {
		if (buttonArray.length > 0) {
			for(var ii = 0; ii < buttonArray.length; ii++) {
				buttonArray[ii].onMouseDown(e);
			}
		}
	}
	
	this.onMouseUp = function(e) {
		if (buttonArray.length > 0) {
			for(var ii = 0; ii < buttonArray.length; ii++) {
				if (buttonArray[ii].onMouseUp(e)) {
					return buttonArray[ii].command;
				}
			}
		}
		
		return menuCommands.null_cmd;
	}
	
	this.displayStartMenu = function() {
		this.remove();
		loadStartMenu();
	}
	
	this.displayHelpMenu = function() {
		this.remove();
		loadHelpMenu();
	}
	
	this.displayCreditsMenu = function() {
		this.remove();
		loadCreditsMenu();
	}
	
	this.displayInGameMenu = function() {
		this.remove();
		loadInGameMenu();
	}
	
	this.displayEndGame = function() {
		this.remove();
		loadEndGameMenu();
	}
	
	function loadStartMenu() {
		var button_x = (width - BUTTON_WIDTH) / 2;
		var button_y = height / 2;
		var gap = 30;
		
		buttonArray.push(new Button(ctx, button_x, button_y, BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_FONT_TYPE, BUTTON_FONT_HEIGHT, BUTTON_FONT_COLOR, BUTTON_FONT_WEIGHT, "START", menuCommands.start_cmd));
		
		button_y += BUTTON_HEIGHT + gap;
		buttonArray.push(new Button(ctx, button_x, button_y, BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_FONT_TYPE, BUTTON_FONT_HEIGHT, BUTTON_FONT_COLOR, BUTTON_FONT_WEIGHT, "INSTRUCTIONS", menuCommands.help_cmd));
		
		button_y += BUTTON_HEIGHT + gap;
		buttonArray.push(new Button(ctx, button_x, button_y, BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_FONT_TYPE, BUTTON_FONT_HEIGHT, BUTTON_FONT_COLOR, BUTTON_FONT_WEIGHT, "CREDITS", menuCommands.credits_cmd));
	}
	
	function loadHelpMenu() {
		// show gameplay info.
		
		helpImage.onload = drawHelpImage;
		helpImage.src = IMAGE_PATH + HELP_IMAGE + IMAGE_TYPE;
	}
	
	function loadCreditsMenu() {
		var text1 = "Development";
		var text2 = "Hapreet Bains";
		var text3 = "Special Thanks";
		var text4 = "Nicholas Verrinder (Nick) for testing";
		var text5 = "Knowledge Source"
		var text6 = "www.w3schools.com, Modern Javascript book, www.stackoverflow.com";
		var text7 = "";
		var text8 = "";
		var text9 = "";
		
		const C_TEXT = 0;
		const C_COLOR = 1;
		
		var creditsArr = [ 
			[text1, RED_COLOR], [text2, ORANGE_COLOR], [text3, RED_COLOR], [text4, ORANGE_COLOR],
			[text5, RED_COLOR], [text6, ORANGE_COLOR], [text7, RED_COLOR], [text8, ORANGE_COLOR], [text9, ORANGE_COLOR] 
		];
		
		var text_x = 0;
		var text_y = 0;
		var gap = 20;
		for (var ii = 0; ii < creditsArr.length; ii++) {
			ctx.fillStyle = creditsArr[ii][C_COLOR];
			text_x = (width - ctx.measureText(creditsArr[ii][C_TEXT]).width) / 2;
			text_y += CREDITS_FONT_HEIGHT + gap;
			ctx.fillText(creditsArr[ii][C_TEXT], text_x, text_y);
		}
		
		var gap = 30;
		var button_x = width - BUTTON_WIDTH - gap;
		var button_y = height - BUTTON_HEIGHT - gap;
		
		buttonArray.push(new Button(ctx, button_x, button_y, BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_FONT_TYPE, BUTTON_FONT_HEIGHT, BUTTON_FONT_COLOR, BUTTON_FONT_WEIGHT, "BACK", menuCommands.back_cmd));
	}
	
	function loadInGameMenu() {
		messageImg = new Image();
		messageImg.onload = drawMessageBG;
		messageImg.src = IMAGE_PATH + MESSAGE_BG + IMAGE_TYPE;
	}
	
	function loadEndGameMenu() {
		var gap = 50;
		var btnWidth = BUTTON_WIDTH;
		var button_x = width - btnWidth - gap;
		var button_y = height - btnWidth - gap;
		
		buttonArray.push(new Button(ctx, width - btnWidth - gap, height - BUTTON_HEIGHT - gap, btnWidth, BUTTON_HEIGHT, BUTTON_FONT_TYPE, BUTTON_FONT_HEIGHT, BUTTON_FONT_COLOR, BUTTON_FONT_WEIGHT, "BACK TO MAIN", menuCommands.back_cmd));
	}
	
	function drawHelpImage() {
		ctx.drawImage(helpImage, 0, 0, width, height);
		var gap = 30;
		var button_x = width - BUTTON_WIDTH - gap;
		var button_y = height - BUTTON_HEIGHT - gap;
		
		buttonArray.push(new Button(ctx, button_x, button_y, BUTTON_WIDTH, BUTTON_HEIGHT, BUTTON_FONT_TYPE, BUTTON_FONT_HEIGHT, BUTTON_FONT_COLOR, BUTTON_FONT_WEIGHT, "BACK", menuCommands.back_cmd));
	}
	
	function drawMessageBG() {
		ctx.drawImage(messageImg, 0, (height - 200) / 2, width, 200);
		
		ctx.font = MESSAGE_FONT;
		ctx.fillStyle = ORANGE_COLOR;
		
		var randMessageId = Math.floor(Math.random() * SUCCESS_MESSAGES.length);
		var text_x = (width - ctx.measureText(SUCCESS_MESSAGES[randMessageId]).width) / 2;
		var text_y = height / 2;
		
		ctx.fillText(SUCCESS_MESSAGES[randMessageId], text_x, text_y);
		
		var gap = 50;
		var btnWidth = BUTTON_WIDTH;
		var button_x = width - btnWidth - gap;
		var button_y = height - btnWidth - gap;
		
		buttonArray.push(new Button(ctx, width - btnWidth - gap, height - BUTTON_HEIGHT - gap, btnWidth, BUTTON_HEIGHT, BUTTON_FONT_TYPE, BUTTON_FONT_HEIGHT, BUTTON_FONT_COLOR, BUTTON_FONT_WEIGHT, "BACK TO MAIN", menuCommands.back_cmd));
		buttonArray.push(new Button(ctx, 0 + gap, height - BUTTON_HEIGHT - gap, btnWidth, BUTTON_HEIGHT, BUTTON_FONT_TYPE, BUTTON_FONT_HEIGHT, BUTTON_FONT_COLOR, BUTTON_FONT_WEIGHT, "NEXT TRACK", menuCommands.next_track_cmd));
	}
	
	function clearCanvas() {
		ctx.clearRect(0, 0, width, height);
	}
}
