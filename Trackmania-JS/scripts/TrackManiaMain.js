console.log("main");

var canvasTrack = document.getElementById("canvasTrack");
var canvasCar = document.getElementById("canvasCar");
var canvasHUD = document.getElementById("canvasHUD");
var canvasCheckpoints = document.getElementById("canvasCheckpoints");
var canvasOverlayBG = document.getElementById("canvasOverlayBG");
var canvasMenu = document.getElementById("canvasMenu");

var contextTrack = canvasTrack.getContext("2d");
var context2dCar = canvasCar.getContext("2d");
var contextHUD = canvasHUD.getContext("2d");
var contextCheckpoints = canvasCheckpoints.getContext("2d");
var contextMenu = canvasMenu.getContext("2d");
var contextOverlayBG = canvasOverlayBG.getContext("2d");

const CANVAS_WIDTH = canvasTrack.width;
const CANVAS_HEIGHT = canvasTrack.height;

const CANVAS_CAR_WIDTH = canvasCar.width;
const CANVAS_CAR_HEIGHT = canvasCar.height;

const CANVAS_HUD_WIDTH = canvasHUD.width;
const CANVAS_HUD_HEIGHT = canvasHUD.height;

const CANVAS_CP_WIDTH = canvasCheckpoints.width;
const CANVAS_CP_HEIGHT = canvasCheckpoints.height;

const CANVAS_OVERLAY_WIDTH = canvasOverlayBG.width;
const CANVAS_OVERLAY_HEIGHT = canvasOverlayBG.height;

const CANVAS_MENU_WIDTH = canvasMenu.width;
const CANVAS_MENU_HEIGHT = canvasMenu.height;

const BG_AUDIO_PATH = "sound/title-sound.mp3";

const KEY_LEFT = 37;
const KEY_UP = 38;
const KEY_RIGHT = 39;
const KEY_DOWN = 40;

const CAR_SIZE = 50;
const BLOCK_SIZE = 100;

const REFRESH_RATE = Math.floor(1000 / 30);

var car = null;
var track = null;
var hud = null;
var checkpoints = null;
var overlay = null;
var menu = null;
var menuCommands = new MenuCommands();
var hudUpdateInterval = null;
var gameRunTime = 0;
var audio = null;

function loadCar() {
	console.log("car spawn: ", track.spawnPoint().x, track.spawnPoint().y);
	car = new Car(context2dCar, track.spawnPoint().x, track.spawnPoint().y, CAR_SIZE, CAR_SIZE, CANVAS_CAR_WIDTH, CANVAS_CAR_HEIGHT);
}

function loadTrack() {
	track = new Track(contextTrack, CANVAS_WIDTH, CANVAS_HEIGHT, BLOCK_SIZE);
	track.loadNextTrack();
}

function loadHUD() {
	hud = new HUD(contextHUD, 0, 0, CANVAS_HUD_WIDTH, CANVAS_HUD_HEIGHT);
	if (hudUpdateInterval == null) {
		hudUpdateInterval = setInterval(update, REFRESH_RATE);
	}
}

function loadCheckpoints() {
	checkpoints = new TrackCheckpoints(contextCheckpoints, 0, 0, CANVAS_CP_WIDTH, CANVAS_CP_HEIGHT, BLOCK_SIZE);
	checkpoints.loadCheckPoints(track.getCurrentTrack());
}

function loadMenu() {
	menu = new Menu(contextMenu, 0, 0, CANVAS_MENU_WIDTH, CANVAS_MENU_HEIGHT);
}

function loadOverlay() {
	overlay = new Overlay(contextOverlayBG, 0, 0, CANVAS_OVERLAY_WIDTH, CANVAS_OVERLAY_HEIGHT);
}

function update() {
	gameRunTime += REFRESH_RATE;
	makeDecisions();
	updateHUD();
}

function makeDecisions() {
	var car_x = car.getPos().coord_x;
	var car_y = car.getPos().coord_x;
	
	if (!track.isOnTrack(track.getCurrentBlock(car.getPos()))) {
		clearEvents();
		car.resetState();
		car = new Car(context2dCar, track.spawnPoint().x, track.spawnPoint().y, CAR_SIZE, CAR_SIZE, CANVAS_CAR_WIDTH, CANVAS_CAR_HEIGHT);
		addEvents();
	}
	else {
		stateMachine( checkpoints.registerCheckPoints(track.getCurrentBlock(car.getPos())) );
	}
}

function updateHUD() {
	hud.update(car.getCurrentSpeed(), car.getCurrentGear(), car.getCurrentRpms(), getElapsedTime(gameRunTime));
}

function playBGAudio() {
	audio = new Audio(BG_AUDIO_PATH);
	audio.loop = true;
	audio.play();
}

function bootUp() {
	playBGAudio();
	addMouseEvents();
	loadOverlay();
	overlay.loadStart();
	loadMenu();
	menu.displayStartMenu();
}

function start() {
	loadTrack();
	loadCar();
	loadCheckpoints();
	loadHUD();
	
	addEvents();
}

function stopGameplay() {
	hudUpdateInterval = _checkClearInterval(hudUpdateInterval);
	car.resetState();
	clearEvents();
	hud.clearHUD();
}

function gotoNextTrack() {
	if (track != null) {
		track.loadNextTrack();
		checkpoints.loadCheckPoints(track.getCurrentTrack());
		loadCar();
		
		gameRunTime = 0;
		if (hudUpdateInterval == null) {
			hudUpdateInterval = setInterval(update, REFRESH_RATE);
		}
		
		addEvents();
	}
}

function stateMachine(cmd) {
	if (cmd == menuCommands.start_cmd) {
		removeMouseEvents();
		menu.remove();
		overlay.remove();
		start();
	}
	else if (cmd == menuCommands.help_cmd) {
		menu.displayHelpMenu();
		overlay.loadMenuBG();
	}
	else if (cmd == menuCommands.back_cmd) {
		menu.displayStartMenu();
		overlay.loadStart();
	}
	else if (cmd == menuCommands.credits_cmd) {
		menu.displayCreditsMenu();
		overlay.loadMenuBG();
	}
	else if (cmd == menuCommands.next_track_cmd) {
		removeMouseEvents();
		menu.remove();
		overlay.remove();
		if (!track.isLastTrack()) {
			gotoNextTrack();
		}
		else {
			overlay.loadEndGame();
			menu.displayEndGame();
		}
	}
	else if (cmd == menuCommands.finish_race_cmd) {
		stopGameplay();
		loadOverlay();
		overlay.loadOverlay();
		menu.displayInGameMenu();
	}
}

function addEvents() {
	document.addEventListener("keydown", car.move);
	document.addEventListener("keyup", car.stopMove);
}

function clearEvents() {
	document.removeEventListener("keydown", car.move);
	document.removeEventListener("keyup", car.stopMove);
}

function addMouseEvents() {
	document.addEventListener("mousemove", onMouseMove, true);
	document.addEventListener("mousedown", onMouseDown, true);
	document.addEventListener("mouseup", onMouseUp, true);
}

function removeMouseEvents() {
	document.removeEventListener("mousemove", onMouseMove);
	document.removeEventListener("mousedown", onMouseDown);
	document.removeEventListener("mouseup", onMouseUp);
}

function onMouseMove(e) {
	if (menu != null) {
		menu.onMouseMove(e);
	}
}

function onMouseDown(e) {
	if (menu != null) {
		menu.onMouseDown(e);
	}
}

function onMouseUp(e) {
	if (menu != null) {
		var btn_cmd = menu.onMouseUp(e);
		stateMachine(btn_cmd);
	}
}

function getElapsedTime(elapsedMS) {
	var milliSeconds = elapsedMS % 1000;
	var totalSeconds = 0;
	var seconds = 0;
	var totalMinutes = 0;
	
	if (elapsedMS > 1000)
	{
		totalSeconds = Math.floor(elapsedMS / 1000);
	}
	
	if (totalSeconds > 59) {
		seconds = totalSeconds % 60;
		totalMinutes = Math.floor(totalSeconds / 60);
	}
	else {
		seconds = totalSeconds;
	}
	
	var minutes = totalMinutes % 60;
	
	return minutes + ":" + seconds;
}

function _checkClearInterval(intervalInstance) {
	console.log("clearing interval");
	if (intervalInstance != null) {
		clearInterval(intervalInstance);
	}
	
	return null;
}
	

bootUp();

function handleKeyPress(event) {
	if (event.keyCode == KEY_LEFT) {
		console.log("key left");
	}
	else if (event.keyCode == KEY_UP) {
		console.log("key up");
	}
	else if (event.keyCode == KEY_RIGHT) {
		console.log("key right");
	}
	else if (event.keyCode == KEY_DOWN) {
		console.log("key down");
	}
}