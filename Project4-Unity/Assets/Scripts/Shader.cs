﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shader : MonoBehaviour
{
	//public MeshFilter meshFilter;

	public Vector3[] newVertices;
	public Vector2[] newUV;
	public int[] newTriangles;	// array size must be multiple of 3

	public float width = 1.0f;
	public float height = 1.0f;

	// Start is called before the first frame update
	void Start()
	{
		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
		mesh.vertices = newVertices;
		mesh.uv = newUV;
		mesh.triangles = newTriangles;

		//CreateRectangleMesh( mesh, new Vector2(0, 0), 2, 2 );
		CreateBoxMesh( mesh, new Vector3(), 2,3,4 );
	}

	// Update is called once per frame
	void Update()
	{
		/*Mesh mesh = GetComponent<MeshFilter>().mesh;
		Vector3[] vertices = mesh.vertices;
		Vector3[] normals = mesh.normals;

		for (var i = 0; i < vertices.Length; i++)
		{
			vertices[i] += normals[i] * Mathf.Sin(Time.time);
		}

		mesh.vertices = vertices;*/
	}

	private void CreateRectangleMesh( Mesh mesh, Vector2 originCoords, float width, float height )
	{
		Vector3[] vertices = new Vector3[4];
		vertices[0] = new Vector3( originCoords.x, 0, originCoords.y );
		vertices[1] = new Vector3( originCoords.x + width, 0, originCoords.y );
		vertices[2] = new Vector3( originCoords.x + width, 0, originCoords.y + height );
		vertices[3] = new Vector3( originCoords.x, 0, originCoords.y + height );

		mesh.vertices = vertices;

		// the clockwise traversal of vertices decides which face of the triangle is front.
		int[] triangles = new int[6];	// number of triangles * 3

		triangles[0] = 0;
		triangles[1] = 3;
		triangles[2] = 1;

		triangles[3] = 3;
		triangles[4] = 2;
		triangles[5] = 1;

		//triangles[3] = 1;
		//triangles[4] = 2;
		//triangles[5] = 3;

		mesh.triangles = triangles;

		mesh.RecalculateNormals();
	}

	private void CreateBoxMesh( Mesh mesh, Vector3 origin, float width, float height, float depth )
	{
		Vector3[] vertices = new Vector3[8];

		// front
		vertices[0] = new Vector3( origin.x, origin.y, origin.z );
		vertices[1] = new Vector3( origin.x, origin.y, origin.z + height );
		vertices[2] = new Vector3( origin.x + width, origin.y, origin.z + height );
		vertices[3] = new Vector3( origin.x + width, origin.y, origin.z );

		//back
		vertices[4] = new Vector3( origin.x, origin.y + depth, origin.z );
		vertices[5] = new Vector3( origin.x, origin.y + depth, origin.z + height );
		vertices[6] = new Vector3( origin.x + width, origin.y + depth, origin.z + height );
		vertices[7] = new Vector3( origin.x + width, origin.y + depth, origin.z );

		mesh.vertices = vertices;

		// the clockwise traversal of vertices decides which face of the triangle is front. Left hand rule
		int[] triangles = new int[]
		{
			0, 1, 2,	/*face front*/
			2, 3, 0,
			4, 7, 6,	/*face back*/
			6, 5, 4, 
			0, 3, 7,	/*face bottom*/
			7, 4, 0,
			1, 5, 6,	/*face top*/
			6, 2, 1,
			0, 4, 5,	/*face left*/
			5, 1, 0,
			7, 6, 2,	/*face right*/
			2, 3, 7
		};	// number of planar faces * 2

		mesh.triangles = triangles;

		mesh.RecalculateNormals();
	}
}
