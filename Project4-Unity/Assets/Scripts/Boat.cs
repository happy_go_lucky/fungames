﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boat : MonoBehaviour
{
	private GameObject _gun;
	private GameObject _pilot;
	private GameObject _hull;

	private Gun _gunScript;
	private Pilot _pilotScript;
	private Hull _hullScript;

	// Start is called before the first frame update
	void Start()
	{
		Debug.Log("Boat:Start");
		_gun = GameObject.Find( "Gun" );
		_pilot = GameObject.Find( "Pilot" );
		_hull = GameObject.Find( "Hull" );

		_gunScript = _gun.GetComponent<Gun>();
		_pilotScript = _pilot.GetComponent<Pilot>();
		_hullScript = _hull.GetComponent<Hull>();
		MainScript.isEnabled = true;
	}

	private void OnEnable()
	{
		
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		
	}

	public void TriggerGun()
	{
		_gunScript.Fire();
	}

	private void CleanUpResources()
	{
		if ( _gun != null ) DestroyObject( _gun );
		if ( _pilot != null ) DestroyObject( _pilot );
		if ( _hull != null ) DestroyObject( _hull );
	}

	private void DestroyObject( GameObject anObject )
	{
		GameObject.Destroy( anObject );
	}

	public Vector3 GetGunPosition()
	{
		return _gun.transform.position;
	}

	public Quaternion GetGunAngle()
	{
		return _gun.transform.rotation;
	}

	public Vector3 GetPilotPosition()
	{
		return _pilot.transform.position;
	}

	public void SetupGun( GunData data )
	{
		_gunScript.UpdateCanonBallVelocity( data.vel );
		//_gunScript.AlignGun( data.vel );
	}
}
