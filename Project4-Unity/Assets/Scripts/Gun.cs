﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
	private const string CANONBALL_PREFAB_PATH = "Prefabs/CanonBall";

	private List<GameObject> _canonBalls;
	private List<CannonBall> _canonBallScripts;

	private float _shot_accel_z;
	private float _shot_accel_y;

	private Vector3 _shot_velocity;

	public enum STATE
	{
		loaded,
		unloaded
	}

	private STATE _state;

	// Called before the script is attached. Do all the initialization here.
	private void Awake()
	{
		_canonBalls = new List<GameObject>();
		_canonBallScripts = new List<CannonBall>();
	}

	// Start is called before the first frame update
	void Start()
	{
		Debug.Log("Gun:Start");
		_state = STATE.unloaded;
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		if ( _state == STATE.loaded )
		{
			// do nothing, wait 
		}

		if ( _state == STATE.unloaded )
		{
			// 
			LoadGun();
		}
	}

	public void LoadGun()
	{
		_canonBalls.Add( CreateObject( CANONBALL_PREFAB_PATH, this.transform.position, new Quaternion() ) );
		_canonBallScripts.Add( _canonBalls[_canonBalls.Count - 1].GetComponent<CannonBall>() );
		_state = STATE.loaded;
	}

	public void Fire()
	{
		_state = STATE.unloaded;
		_canonBallScripts[_canonBalls.Count - 1].UpdateVelocity( _shot_velocity );
		_canonBallScripts[_canonBalls.Count - 1].SetState( CannonBall.STATE.airborne );
		//_canonBallScripts[_canonBalls.Count - 1].UpdateAngles( this.transform.rotation );
	}

	public void DestroyCannonBall()
	{
		int ii = 0;
		while ( ( ii < _canonBalls.Count ) && ( _canonBallScripts[ii].GetState() == CannonBall.STATE.hit ) )
		{
			DestroyObject( _canonBalls[ii] );
			_canonBalls.RemoveAt( ii );
			_canonBallScripts.RemoveAt( ii );
			ii++;
		}
	}

	private GameObject CreateObject( string objectPath, Vector3 position, Quaternion rotation )
	{
		return Instantiate ( Resources.Load<GameObject>( objectPath ), position, rotation ) as GameObject;
	}

	public void AlignGun( Vector3 vel )
	{
		//_canonBallScripts[_canonBalls.Count - 1].UpdateVel( vel );
		//this.transform.rotation = rotation;
	}

	private void DestroyObject( GameObject anObject )
	{
		GameObject.Destroy( anObject );
	}

	public void UpdateCanonBallVelocity( Vector3 vel )
	{
		_shot_velocity = vel;
	}
}
