﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainScript : MonoBehaviour
{
	private const string BOAT_PREFAB_PATH = "Prefabs/Boat";
	private const string TARGET_PREFAB_PATH = "Prefabs/Target";
	private const string CANONBALL_PREFAB_PATH = "Prefabs/CanonBall";
	
	public static readonly float BOAT_TURN_SPEED = 0.01f;
	public static readonly float BOAT_MOVE_SPEED = 0.8f;
	public static readonly float GRAVITY = -9.81f;
	public static readonly float BALL_STOP_THRESHOLD_Y = 0.05f;

	[Header("Range To Target")]
	public float rangeToTarget_z;
	public float rangeToTarget_x;

	public float canonBallSpeed;
	
	[Header("Gun Angle")]
	public float gunAngle_theta_radians;	// should be 27.9673642122666 for speed = 15
	public float gunAngle_theta_degrees;
	public float gunAngle_sin;
	public float gunAngle_cos;

	[Header("Angle alpha")]
	public float alpha_deg;
	public float alpha_rad;
	public float sin_alpha;
	public float cos_alpha;

	[Header("Angle gamma")]
	public float gamma_deg;
	public float gamma_rad;
	public float sin_gamma;
	public float cos_gamma;

	[Header("Time stats")]
	public float computedTime;
	public float actualTime;

	[Header("Velocity stats")]
	private float velocity_y;
	private float velocity_z;
	public Vector3 velocity;

	private GameObject _boat;
	private GameObject _target;

	private Boat _boatScript;
	private Target _targetScript;

	private GameInput _gameInput;

	private GunData _gunData;

	//private Renderer _canonBallRenderer;

	private float _accel_z;
	private float _accel_y;

	public static bool isEnabled = false;

	private void Awake()
	{
		canonBallSpeed = 14.0f;
	}

	// Start is called before the first frame update
	private void Start()
	{
		Debug.Log("MainScript:Start");
		
		InitScene();
		AttachInput();

		//_canonBallRenderer = _canonBall.GetComponent<Renderer>();
	}

	private void HackyInit()
	{
		UpdateTargetDistance();
		ComputeGunAngle();
		ComputeShotVelocity();
		UpdateBoatData();
		ComputeTime();

		_boatScript.SetupGun( _gunData );
	}

	// Update is called once per frame
	private void FixedUpdate()
	{
		if ( isEnabled )
		{
			isEnabled = false;
			HackyInit();
		}
		
		_gameInput.UpdateInput();
		
		//ComputeTime();
	}

	private void InitScene()
	{
		_boat = CreateObject( BOAT_PREFAB_PATH, new Vector3( 0, 0, 0 ), new Quaternion() );	// gun pos = -4
		_target = CreateObject( TARGET_PREFAB_PATH, new Vector3( 3, 0, 13 ), new Quaternion() );

		_boatScript = _boat.GetComponent<Boat>();
		_targetScript = _target.GetComponent<Target>();
	}

	private void CleanUpResources()
	{
		if ( _boat != null ) DestroyObject( _boat );
		if ( _target != null ) DestroyObject( _target );
	}

	private GameObject CreateObject( string objectPath, Vector3 position, Quaternion rotation )
	{
		return Instantiate ( Resources.Load<GameObject>( objectPath ), position, rotation ) as GameObject;
	}

	private void DestroyObject( GameObject anObject )
	{
		GameObject.Destroy( anObject );
	}

	private void UpdateTargetDistance()
	{
		rangeToTarget_z = Mathf.Abs( _target.transform.position.z - _boatScript.GetGunPosition().z );
		rangeToTarget_x = Mathf.Abs( _target.transform.position.x - _boatScript.GetGunPosition().x );
	}

	private void AttachInput()
	{
		_gameInput = GameInput.GetInstance();
		_gameInput.AttachInput( 
			actionClick: OnActionKey,
			leftClick: OnLeftKey,
			rightClick: OnRightKey,
			downClick: OnDownKey,
			upClick: OnUpKey
		);
	}

	private void ComputeGunAngle()
	{
		gunAngle_theta_radians = Mathf.Abs( Mathf.Asin( ( GRAVITY * rangeToTarget_z ) / Mathf.Pow( canonBallSpeed, 2.0f ) ) / 2 );
		gunAngle_theta_degrees = gunAngle_theta_radians * 180 / Mathf.PI;
		gunAngle_sin = Mathf.Sin( gunAngle_theta_radians );
		gunAngle_cos = Mathf.Cos( gunAngle_theta_radians );

		float sqrt_x_sq_plus_z_sq = Mathf.Sqrt( Mathf.Pow( rangeToTarget_z, 2.0f ) + Mathf.Pow( rangeToTarget_x, 2.0f ) );
		alpha_rad = Mathf.Asin( -GRAVITY * sqrt_x_sq_plus_z_sq / Mathf.Pow( canonBallSpeed, 2.0f ) ) / 2;
		alpha_deg = 90 - ( alpha_rad * 180 / Mathf.PI );
		sin_alpha = Mathf.Sin( alpha_deg * Mathf.PI / 180 );
		cos_alpha = Mathf.Cos( alpha_deg * Mathf.PI / 180 );

		gamma_rad = Mathf.Asin( rangeToTarget_x / sqrt_x_sq_plus_z_sq );
		gamma_deg = gamma_rad * 180 / Mathf.PI;
		sin_gamma = Mathf.Sin( gamma_rad );
		cos_gamma = Mathf.Cos( gamma_rad );
	}

	private void ComputeTime()
	{
		//time = Range / ( Speed * Cos theta )
		computedTime = rangeToTarget_z / ( canonBallSpeed * gunAngle_cos );
	}

	private void ComputeShotVelocity()
	{
		velocity_y = canonBallSpeed * gunAngle_sin;
		velocity_z = canonBallSpeed * gunAngle_cos;
	}

	private void UpdateBoatData()
	{
		velocity = new Vector3( canonBallSpeed * cos_gamma * sin_alpha,
								canonBallSpeed * cos_alpha,
								canonBallSpeed * sin_gamma * sin_alpha );
		_gunData = new GunData( velocity );
	}

	private void OnUpKey()
	{
		
	}

	private void OnDownKey()
	{

	}

	private void OnLeftKey()
	{

	}

	private void OnRightKey()
	{

	}

	private void OnActionKey()
	{
		Debug.Log( "Action button" );
		//launch cannon
		_boatScript.TriggerGun();
	}
}
