﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
	public static readonly Color NORMAL_COLOR = Color.white;
	public static readonly Color HIT_COLOR = Color.red;

	public enum STATE
	{
		stationary,
		airborne,
		hit
	}

	private STATE _state;

	private float _shot_accel_z;
	private float _shot_accel_y;

	public float finalVelocty_x;
	public float finalVelocty_y;
	public float finalVelocty_z;

	public float _shot_velocity_y;
	public float _shot_velocity_z;
	public float _shot_velocity_x;

	private float _displacement_z;
	private float _displacement_y;
	private float _displacement_x;
	private float _displacemntInterval;

	private Vector3 _initPosition;

	public float flightTime;

	public float _alpha;
	public float _gamma;

	private Material _material;

	private void Awake()
	{
		flightTime = 0;
		_state = STATE.stationary;
	}

	// Start is called before the first frame update
	void Start()
	{
		_material = GetComponent<Renderer>().material;
		ChangeColor( NORMAL_COLOR );

		_initPosition = this.transform.position;
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		if ( _state == STATE.airborne )
		{
			Move();
		}
		if ( _state == STATE.stationary )
		{
			// do something
		}
		if ( _state == STATE.hit )
		{
			//
		}
	}

	public void SetState( STATE state )
	{
		_state = state;
	}

	public STATE GetState()
	{
		return _state;
	}

	private void Move()
	{
		_displacemntInterval = Time.deltaTime;
		_shot_accel_z = 0;
		_shot_accel_y = MainScript.GRAVITY;
		
		_displacement_y = this.transform.position.y + ( _shot_velocity_y * _displacemntInterval ) + ( 0.5f * _shot_accel_y * Mathf.Pow( _displacemntInterval, 2f ) );
		_displacement_x = this.transform.position.x + _shot_velocity_z * _displacemntInterval;
		_displacement_z = this.transform.position.z + _shot_velocity_x * _displacemntInterval;
		
		// calculate the updated velocity based on acceleration vf = vi + at
		_shot_velocity_y = _shot_velocity_y + ( _shot_accel_y * _displacemntInterval );
		//_shot_velocity_z = _shot_velocity_z + ( _shot_accel_z * _displacemntInterval );
		//_shot_velocity_x = _shot_velocity_x + ( _shot_accel_z * _displacemntInterval );

		if ( _displacement_y <= MainScript.BALL_STOP_THRESHOLD_Y )
		{
			_state = STATE.hit;
			ChangeColor( HIT_COLOR );
			finalVelocty_x = _shot_velocity_x;
			finalVelocty_y = _shot_velocity_y;
			finalVelocty_z = _shot_velocity_z;
		}
		else
		{
			this.transform.position = new Vector3( _displacement_x, _displacement_y , _displacement_z );
			flightTime += _displacemntInterval;
		}
	}

	private void ChangeColor( Color color )
	{
		_material.color = color;
	}

	public void UpdateVelocity( Vector3 vel )
	{
		_shot_velocity_y = vel.y;
		_shot_velocity_z = vel.z;
		_shot_velocity_x = vel.x;
	}

	public void UpdateAngles( float alpha, float gamma )
	{
		_alpha = alpha;
		_gamma = gamma;
	}
}
