﻿using UnityEngine;

public struct GunData
{
	public Vector3 vel;

	public GunData( Vector3 velocity )
	{
		vel = velocity;
	}
}
