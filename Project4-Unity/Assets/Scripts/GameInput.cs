﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// singleton class
public class GameInput
{
	// Names of each input according to the editor's input manager
	public static readonly string ACTION_BUTTON = "Action";
	public static readonly string JOY_X = "Horizontal";
	public static readonly string JOY_Y = "Vertical";

	// These delegates must be set by objects/scenes expecting user input
	public delegate void ActionButtonClickHandler();
	public delegate void RightButtonClickHandler();
	public delegate void LeftButtonClickHandler();
	public delegate void UpButtonClickHandler();
	public delegate void DownButtonClickHandler();

	private static ActionButtonClickHandler _onActionClick = null;
	private static RightButtonClickHandler _onRightClick = null;
	private static LeftButtonClickHandler _onLeftClick = null;
	private static UpButtonClickHandler _onUpClick = null;
	private static DownButtonClickHandler _onDownClick = null;

	private static GameInput _instance = null;

	private GameInput(  )
	{

	}

	// Return the instance of the controller
	// Use this to get access to the controller
	public static GameInput GetInstance()
	{
		if( _instance == null )
		{
			_instance = new GameInput();
		}

		return _instance;
	}

	// Binds the input to a scene
	public void AttachInput( 
		ActionButtonClickHandler actionClick = null,
		RightButtonClickHandler rightClick = null, 
		LeftButtonClickHandler leftClick = null,
		UpButtonClickHandler upClick = null, 
		DownButtonClickHandler downClick = null )
	{
		_onActionClick = actionClick;
		_onRightClick = rightClick;
		_onLeftClick = leftClick;
		_onUpClick = upClick;
		_onDownClick = downClick;
	}

	// Removes the input binding
	public void DetachInput()
	{
		_onActionClick = null;
		_onRightClick = null;
		_onLeftClick = null;
		_onUpClick = null;
		_onDownClick = null;
	}

	public void UpdateInput()
	{
		if ( Input.GetAxis( JOY_X ) < 0 && _onLeftClick != null ) _onLeftClick();
		if ( Input.GetAxis( JOY_X ) > 0 && _onRightClick != null ) _onRightClick();
		if ( Input.GetAxis( JOY_Y ) < 0 && _onUpClick != null ) _onUpClick();
		if ( Input.GetAxis( JOY_Y ) > 0 && _onDownClick != null ) _onDownClick();
		if ( Input.GetButtonDown( ACTION_BUTTON ) && _onActionClick != null ) _onActionClick();
	}
}
