﻿Shader "Custom/HullShader"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		Pass
		{
			CGPROGRAM
		
			#pragma vertex vertexRoutine
			#pragma fragment fragmentRoutine
			
			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			// color from the material
            fixed4 _Color;

			struct VertexDataIn
			{
				float4 vertex:POSITION;
				float4 color:COLOR;
			};

			struct FragmentDataIn
			{
				float4 vertex:POSITION;
				float4 color:COLOR;
			};

			struct FragmentDataOut
			{
				float4 color:COLOR;
			};

			FragmentDataIn vertexRoutine( VertexDataIn vDataIn )
			{
				FragmentDataIn fDataIn;
				fDataIn.vertex = UnityObjectToClipPos( vDataIn.vertex );
				fDataIn.color = _Color;
				fDataIn.color.a = 0.1f;
				return fDataIn;
			}

			FragmentDataOut fragmentRoutine( FragmentDataIn fDataIn )
			{
				FragmentDataOut fDataOut;
				fDataOut.color = fDataIn.color;
				return fDataOut;
			}
		
			ENDCG
		}
	}
	FallBack "Diffuse"
}
