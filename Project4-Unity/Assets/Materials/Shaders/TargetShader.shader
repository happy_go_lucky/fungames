﻿Shader "Custom/HullShader"
{
	Properties
	{
		_Color1( "Color1", Color ) = ( 1, 0, 0, 0.3 )
		_Color2( "Color2", Color ) = ( 1, 1, 1, 0.3 )
		_Scale( "Pattern Size", Range( 0, 1 ) ) = 1
	}
	SubShader
	{
		Tags
		{
			"RenderType"="Transparent"
			"Queue"="Transparent" 
		}
		Pass
		{
			CGPROGRAM
			#include "UnityCG.cginc"
			// the process is vertex shader -> clipper and primitive assembler -> rasteriser -> pixel/fragment shader 
			/**
			- vertex defines a point in a mesh of the object being created
			- a set of vertices are used to form primitive shapes like triangles or polygons and are clipped to fit into view window
			- the vertices/primitive shapes are then passed to the rasterizer to compute the pixels that are present between them
			- the computed pixel data is then passed to the fragment/pixel shader which deals with color and depth etc. and updates the frame buffer
			**/

			#pragma vertex vertexRoutine
			#pragma fragment fragmentRoutine
			
			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			// color from the material
            fixed4 _Color1;
			fixed4 _Color2;
			float _Scale;

			struct VertexDataIn
			{
				float4 vertex:POSITION;
				float4 color:COLOR;
			};

			struct FragmentDataIn
			{
				float4 vertex:POSITION;
				float3 worldPos:TEXCOORD0;
				float4 color:COLOR;
			};

			struct FragmentDataOut
			{
				float4 color:COLOR;
			};

			FragmentDataIn vertexRoutine( VertexDataIn vDataIn )
			{
				FragmentDataIn fDataIn;
				fDataIn.vertex = UnityObjectToClipPos( vDataIn.vertex );
				fDataIn.worldPos = mul( unity_ObjectToWorld, vDataIn.vertex );	// mul multiplies two matrices. Row * columns and return a matrix
				fDataIn.color = vDataIn.color;
				return fDataIn;
			}

			FragmentDataOut fragmentRoutine( FragmentDataIn fDataIn )
			{
				FragmentDataOut fDataOut;

				/*
				float3 temp = floor( fDataIn.worldPos / _Scale );
				if ( temp.y % 2 == 0 )
				{
					fDataOut.color = _Color1;
				}
				else
				{
					fDataOut.color = _Color2;
				}
				*/
				fDataOut.color.rgb = fDataIn.worldPos;
				return fDataOut;
			}
		
			ENDCG
		}
	}
	FallBack "Diffuse"
}
